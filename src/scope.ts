import { Check } from '@typemon/check';
import { AsyncHook, createHook, executionAsyncId } from 'async_hooks';

const scopes: Map<number, Chain> = new Map();
const parents: Map<number, number> = new Map();
const childrens: Map<number, Set<number>> = new Map();
const destroyables: Set<number> = new Set();
const hook: AsyncHook = createHook({
    init,
    destroy
});

function getById(id: number): null | Chain {
    return scopes.get(id) ?? null;
}

function init(id: number, _: never, parent: number): void {
    const scope: null | Chain = getById(parent);

    if (Check.isNull(scope)) {
        return;
    }

    const siblings: Set<number> = childrens.get(parent) ?? new Set();

    childrens.set(parent, siblings.add(id));
    parents.set(id, parent);
    scopes.set(id, scope);
}

function destroy(id: number, fromChildren: boolean = false): void {
    const scope: undefined | Chain = scopes.get(id);

    if (Check.isUndefined(scope)) {
        return;
    }

    if (fromChildren && Check.isFalse(destroyables.has(id))) {
        return;
    }
    else {
        destroyables.add(id);
    }

    const children: undefined | Set<number> = childrens.get(id);

    if (Check.isNotUndefined(children) && Check.greater(children.size, 0)) {
        return;
    }

    const parent: undefined | number = parents.get(id);

    if (Check.isNotUndefined(parent) && childrens.has(parent)) {
        const siblings: Set<number> = childrens.get(parent) as Set<number>;

        siblings.delete(id);
    }

    destroyables.delete(id);
    childrens.delete(id);
    parents.delete(id);
    scopes.delete(id);

    if (Check.equal(id, scope.id)) {
        scope.destroy();
    }

    if (Check.isNotUndefined(parent)) {
        destroy(parent, true);
    }
}

export class Chain implements Scope {
    public readonly id: number;
    public readonly root: Scope;

    private destroyed: boolean;
    private readonly destroyCallbacks: Array<Scope.Callback>;

    public constructor(
        public readonly name: string,
        public readonly parent: null | Scope,
    ) {
        this.id = executionAsyncId();
        this.root = this.parent?.root ?? this;

        this.destroyed = false;
        this.destroyCallbacks = [];
    }

    public destroy(): void {
        this.destroyed = true;

        for (const callback of this.destroyCallbacks) {
            callback(this);
        }
    }

    public onDestroy(callback: Scope.Callback): void {
        if (this.destroyed) {
            throw new Error('The scope is destroyed.');
        }

        this.destroyCallbacks.push(callback);
    }
}

export interface Scope {
    readonly name: string;
    readonly id: number;
    readonly parent: null | Scope;
    readonly root: Scope;

    onDestroy(callback: Scope.Callback): void;
}
export namespace Scope {
    export type Callback = (scope: Scope) => void;

    function getByName(name: string, scope: null | Scope): null | Scope {
        if (Check.isNull(scope)) {
            return null;
        }

        if (Check.equal(name, scope.name)) {
            return scope;
        }

        return getByName(name, scope.parent);
    }

    export function get(name?: string): null | Scope {
        const scope: null | Scope = getById(executionAsyncId());

        if (Check.isUndefined(name)) {
            return scope;
        }

        return getByName(name, scope);
    }

    export function run<Result>(name: string, callback: (scope: Scope) => Result | Promise<Result>): Promise<Result> {
        const parentId: number = executionAsyncId();
        const parent: null | Scope = getById(parentId);

        return new Promise((resolve: (result: Result | Promise<Result>) => void): void => {
            process.nextTick((): void => {
                const scope: Chain = new Chain(name, parent);

                if (Check.equal(scopes.size, 0)) {
                    hook.enable();
                }

                scopes.set(scope.id, scope);
                scope.onDestroy((): void => {
                    if (Check.equal(scopes.size, 0)) {
                        hook.disable();
                    }
                });

                resolve(callback(scope));
            });
        });
    }
}
