import { Scope, Chain } from './scope';

test('without', (): void => expect(Scope.get()).toBeNull());

test('within', async (): Promise<void> => {
    await Scope.run('a', async (a: Scope): Promise<void> => {
        expect(Scope.get('null')).toBeNull();
        expect(Scope.get('a')).toBe(a);
        expect(Scope.get()).toBe(a);
        expect(a.name).toBe('a');
        expect(a.parent).toBeNull();
        expect(a.root).toBe(a);

        await Scope.run('b', async (b: Scope): Promise<void> => {
            expect(Scope.get('null')).toBeNull();
            expect(Scope.get('a')).toBe(a);
            expect(Scope.get('b')).toBe(b);
            expect(Scope.get()).toBe(b);
            expect(b.name).toBe('b');
            expect(b.parent).toBe(a);
            expect(b.root).toBe(a);

            await Scope.run('c', async (c: Scope): Promise<void> => {
                expect(Scope.get('null')).toBeNull();
                expect(Scope.get('a')).toBe(a);
                expect(Scope.get('b')).toBe(b);
                expect(Scope.get('c')).toBe(c);
                expect(Scope.get()).toBe(c);
                expect(c.name).toBe('c');
                expect(c.parent).toBe(b);
                expect(c.root).toBe(a);
            });
        });
    });
});

test('destroy', (): void => {
    const chain: Chain = new Chain('default', null);
    const callback: Scope.Callback = jest.fn((): void => { });

    chain.onDestroy(callback);
    chain.destroy();

    expect(callback).toBeCalled();
    expect((): void => chain.onDestroy(callback)).toThrow('The scope is destroyed.');
});
