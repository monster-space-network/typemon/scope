# [5.1.0](https://gitlab.com/monster-space-network/typemon/scope/compare/5.0.2...5.1.0) (2021-02-13)


### Features

* 파괴 로직 변경 ([0e1faa4](https://gitlab.com/monster-space-network/typemon/scope/commit/0e1faa403b6e01125c0ba4e63afeed67228e477f))



## [5.0.2](https://gitlab.com/monster-space-network/typemon/scope/compare/5.0.1...5.0.2) (2021-02-13)


### Bug Fixes

* 중간 파괴 문제 해결 ([5c478a0](https://gitlab.com/monster-space-network/typemon/scope/commit/5c478a0ccb48ff4ee5304f06b77c982d5881c6a9))



## [5.0.1](https://gitlab.com/monster-space-network/typemon/scope/compare/5.0.0...5.0.1) (2021-02-13)


### Bug Fixes

* 유효한 범위 파괴 문제 해결 및 리팩터링 ([1c17b0a](https://gitlab.com/monster-space-network/typemon/scope/commit/1c17b0aa1c25d57a85b6e046ee9a960e6ed898da))



# [5.0.0](https://gitlab.com/monster-space-network/typemon/scope/compare/4.0.0...5.0.0) (2020-12-24)


### Features

* 재작성 ([1765627](https://gitlab.com/monster-space-network/typemon/scope/commit/1765627bdc639d756d9d1f040b409f2d16d125c7))



# [4.0.0](https://gitlab.com/monster-space-network/typemon/scope/compare/3.0.0...4.0.0) (2020-08-20)


### Bug Fixes

* **getById:** 잘못된 검색 방식 수정 ([98cfe82](https://gitlab.com/monster-space-network/typemon/scope/commit/98cfe821f64803a4d26f98d14ddc8e1412d044cc))


### Features

* get 함수 변경 ([32cfe7f](https://gitlab.com/monster-space-network/typemon/scope/commit/32cfe7f06d64b13404566d0e77742a7745451160))
* has, hasNot 함수 삭제 ([a9e4e0a](https://gitlab.com/monster-space-network/typemon/scope/commit/a9e4e0aafb937bd0696f368e87a2e0de0aad699d))
* 스코프 생성 함수 개선 ([539cd40](https://gitlab.com/monster-space-network/typemon/scope/commit/539cd4060cdd9d4dc3372e3cd3ce524fe6f65ac0))
* 의존성 업데이트 ([15af4b8](https://gitlab.com/monster-space-network/typemon/scope/commit/15af4b843a1d47c006e244fa6db56c277168b46d))



# [3.0.0](https://gitlab.com/monster-space-network/typemon/scope/compare/2.0.0...3.0.0) (2020-01-16)


### Features

* 의존성 업데이트 ([0536699](https://gitlab.com/monster-space-network/typemon/scope/commit/053669944ce67cdd15c7b319d99b5365ad1eb53c))
* 재작성 ([f349e86](https://gitlab.com/monster-space-network/typemon/scope/commit/f349e862986361198dc1716247747ddead02f063))



# [2.0.0](https://gitlab.com/monster-space-network/typemon/scope/compare/1.0.0...2.0.0) (2019-11-17)


### Features

* 기능 단순화 및 리팩터링 ([db1130c](https://gitlab.com/monster-space-network/typemon/scope/commit/db1130cc67e03556d65ae66a17407ea18cbaf8c1))
* 콜백 파라미터 추가 및 리팩터링 ([11ede0f](https://gitlab.com/monster-space-network/typemon/scope/commit/11ede0f36c4ba8392f8f15920b3a06f97e04c8fd))
* 파괴 콜백 파라미터 추가 ([337d3b2](https://gitlab.com/monster-space-network/typemon/scope/commit/337d3b25cbabfcc18987806f0f41c7c8f9b86ac7))



